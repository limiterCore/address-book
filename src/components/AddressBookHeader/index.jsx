import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';

// Actions
import { addContact } from 'actions/address_actions';

// Styles
import './styles.css';

const AddressBookHeader = ({ addContact, title, className }) => (
  <div
    className={cx('address-book-header', {
      [className]: className,
    })}
  >
    <button
      className="address-book-header__add-button"
      type="button"
      onClick={addContact}
      aria-label="Add contact"
    >
      +
    </button>
    <h2 className="address-book-header__title">{title}</h2>
  </div>
);

AddressBookHeader.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  addContact: PropTypes.func.isRequired,
};

AddressBookHeader.defaultProps = {
  className: null,
};

const mapDispatchToProps = dispatch => ({
  addContact: () => {
    dispatch(addContact());
  },
});

export default connect(
  null,
  mapDispatchToProps,
)(AddressBookHeader);
