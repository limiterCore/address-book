import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import { editContactField, deleteContact } from 'actions/address_actions';

// Misc
import { throttle } from 'helpers/helpers';

// Styles
import './styles.css';

class Entry extends React.Component {
  static propTypes = {
    currentEntry: PropTypes.shape({
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      address: PropTypes.string,
    }).isRequired,
    dispatch: PropTypes.func.isRequired,
    justCreated: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.throttledHandleFieldChange = throttle(this.handleFieldChange, 400);

    this.state = {
      isDeleting: false,
    };
  }

  componentDidMount() {
    const { justCreated } = this.props;

    if (justCreated && this.firstNameInput) {
      this.firstNameInput.focus();
    }
  }

  componentDidUpdate() {
    const { justCreated } = this.props;

    if (justCreated && this.firstNameInput) {
      this.firstNameInput.focus();
    }
  }

  handleFieldChange = (fieldName, value) => {
    const { dispatch, currentEntry } = this.props;
    dispatch(editContactField(currentEntry.id, fieldName, value));
  };

  handleDeleteButtonClick = () => {
    this.setState({
      isDeleting: true,
    });
  };

  deleteEntry = () => {
    const { currentEntry, dispatch } = this.props;

    dispatch(deleteContact(currentEntry.id));
  };

  cancelDelete = () => {
    this.setState({
      isDeleting: false,
    });
  };

  render() {
    const { currentEntry } = this.props;
    const { firstName, lastName, address } = currentEntry;
    const { isDeleting } = this.state;

    return (
      <div className="entry" key={`entry_${currentEntry.id}`}>
        <label className="entry__row" htmlFor="firstName">
          <span className="entry__row-label">First name</span>
          <input
            className="entry__row-input"
            type="text"
            defaultValue={firstName}
            onChange={e => {
              this.throttledHandleFieldChange('firstName', e.target.value);
            }}
            id="firstName"
            ref={node => {
              this.firstNameInput = node;
            }}
          />
        </label>

        <label className="entry__row" htmlFor="lastName">
          <span className="entry__row-label">Last name</span>
          <input
            className="entry__row-input"
            type="text"
            defaultValue={lastName}
            onChange={e => {
              this.throttledHandleFieldChange('lastName', e.target.value);
            }}
            id="lastName"
          />
        </label>

        <label className="entry__row" htmlFor="address">
          <span className="entry__row-label">Address</span>
          <input
            className="entry__row-input"
            type="text"
            defaultValue={address}
            onChange={e => {
              this.throttledHandleFieldChange('address', e.target.value);
            }}
            id="address"
          />
        </label>

        <div className="entry__button-holder">
          {!isDeleting ? (
            <button
              type="button"
              onClick={this.handleDeleteButtonClick}
              className="entry__button entry__button_danger"
            >
              Delete
            </button>
          ) : (
            <React.Fragment>
              <button
                type="button"
                onClick={this.deleteEntry}
                className="entry__button entry__button_danger"
              >
                Yes, I&#39;m sure
              </button>
              <button type="button" onClick={this.cancelDelete} className="entry__button">
                No
              </button>
            </React.Fragment>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentEntry: state.address.currentEntry,
  justCreated: state.address.justCreated,
});

export default connect(mapStateToProps)(Entry);
