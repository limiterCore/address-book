import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Components
import AddressBookHeader from 'components/AddressBookHeader';
import ContactList from 'components/ContactList';
import ContactFilter from 'components/ContactFilter';
import Entry from 'components/Entry';

// Styles
import './styles.css';

const AddressBook = ({ currentEntry }) => (
  <div className="address-book">
    <div className="address-book__left">
      <AddressBookHeader className="address-book__header" title="Address book" />
      <ContactFilter className="address-book__contact-filter" />
      <ContactList className="address-book__contact-list" />
    </div>
    <div className="address-book__right">
      {currentEntry && <Entry className="address-book__entry" />}
    </div>
  </div>
);

AddressBook.propTypes = {
  currentEntry: PropTypes.shape({}),
};

AddressBook.defaultProps = {
  currentEntry: null,
};

const mapStateToProps = state => ({
  currentEntry: state.address.currentEntry,
});

export default connect(mapStateToProps)(AddressBook);
