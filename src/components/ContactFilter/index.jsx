import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import { setFilterValue } from 'actions/address_actions';

// Misc
import { throttle } from 'helpers/helpers';

// Styles
import './styles.css';

class ContactFilter extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    className: PropTypes.string,
    filterValue: PropTypes.string,
  };

  static defaultProps = {
    className: null,
    filterValue: '',
  };

  constructor(props) {
    super(props);
    this.throttledSetFilterValue = throttle(this.setFilterValueLocal, 500);
  }

  setFilterValueLocal = filterValue => {
    const { dispatch } = this.props;
    dispatch(setFilterValue(filterValue));
  };

  render() {
    const { className, filterValue } = this.props;

    return (
      <div
        className={cx('contact-filter', {
          [className]: className,
        })}
      >
        <input
          className={cx('contact-filter__input', {
            'contact-list__item_active': false,
          })}
          onChange={e => {
            this.throttledSetFilterValue(e.target.value);
          }}
          defaultValue={filterValue}
          placeholder="Search"
          aria-placeholder="Filter contacts"
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.address.contacts,
  currentEntry: state.address.currentEntry,
  filterValue: state.address.filterValue,
});

export default connect(mapStateToProps)(ContactFilter);
