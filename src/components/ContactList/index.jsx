import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import { selectContact, getContacts } from 'actions/address_actions';

// Styles
import './styles.css';

class ContactList extends React.Component {
  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(getContacts());
  }

  /**
   * Control focus by keyboard arrows
   */
  handleKeyDown = event => {
    const { contacts } = this.props;
    if (!contacts.length) {
      return;
    }
    const { activeElement } = document;

    if (event.key === 'ArrowDown' || event.key === 'ArrowRight') {
      if (activeElement.nextElementSibling) {
        this.focusedElement = activeElement.nextElementSibling;
      } else {
        this.focusedElement = this.parent.firstElementChild;
      }
      this.focusedElement.focus();
    }

    if (event.key === 'ArrowUp' || event.key === 'ArrowLeft') {
      if (activeElement.previousElementSibling) {
        this.focusedElement = activeElement.previousElementSibling;
      } else {
        this.focusedElement = this.parent.lastElementChild;
      }
      this.focusedElement.focus();
    }
  };

  /**
   * Pass the focus to the first inner element
   * when receive it on parent
   */
  handleFocus = event => {
    const { contacts } = this.props;
    const firstElement = document.querySelector('.contact-list__item');

    if (event.target.tabIndex === 0 && contacts.length) {
      if (this.focusedElement) {
        this.focusedElement.focus();
      } else {
        firstElement.focus();
      }
    }
  };

  render() {
    const { contacts, dispatch, className, currentEntry, filterValue } = this.props;
    if (!contacts || !contacts.length) {
      return null;
    }
    return (
      <div
        role="button"
        className={cx('contact-list', {
          [className]: className,
        })}
        tabIndex={0}
        onKeyDown={this.handleKeyDown}
        onFocusCapture={this.handleFocus}
        ref={node => {
          this.parent = node;
        }}
        aria-label="Contact list"
      >
        {contacts
          .filter(item => {
            if (filterValue) {
              return `${item.firstName} ${item.lastName}`.toLowerCase().indexOf(filterValue) > -1;
            }
            return true;
          })
          .map(item => (
            <button
              type="button"
              key={item.id}
              className={cx('contact-list__item', {
                'contact-list__item_active': currentEntry && currentEntry.id === item.id,
              })}
              onClick={() => {
                dispatch(selectContact(item.id));
              }}
              tabIndex={-1}
            >
              {`${item.firstName} ${item.lastName}`}
            </button>
          ))}
      </div>
    );
  }
}

ContactList.propTypes = {
  dispatch: PropTypes.func.isRequired,
  contacts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
    }),
  ).isRequired,
  currentEntry: PropTypes.shape({
    id: PropTypes.string,
  }),
  className: PropTypes.string,
  filterValue: PropTypes.string.isRequired,
};

ContactList.defaultProps = {
  currentEntry: null,
  className: null,
};

const mapStateToProps = state => ({
  contacts: state.address.contacts,
  currentEntry: state.address.currentEntry,
  filterValue: state.address.filterValue,
});

export default connect(mapStateToProps)(ContactList);
