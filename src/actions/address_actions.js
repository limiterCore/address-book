import * as types from 'constants/action_types';

export const getContacts = () => {
  let contacts;
  try {
    contacts = JSON.parse(window.localStorage.getItem('contacts'));
  } catch (error) {
    contacts = null;
  }
  return {
    type: types.GET_CONTACTS,
    payload: {
      contacts,
    },
  };
};

export const editContactField = (contactId, fieldName, fieldValue) => ({
  type: types.EDIT_CONTACT_FIELD,
  payload: {
    contactId,
    fieldName,
    fieldValue,
  },
});

export const selectContact = contactId => ({
  type: types.SELECT_CONTACT,
  payload: {
    contactId,
  },
});

export const addContact = () => dispatch => {
  dispatch({
    type: types.ADD_CONTACT,
  });
  setTimeout(() => {
    dispatch({
      type: types.RESET_JUST_CREATED,
    });
  }, 100);
};

export const setFilterValue = filterValue => ({
  type: types.SET_FILTER_VALUE,
  payload: {
    filterValue,
  },
});

export const deleteContact = contactId => ({
  type: types.DELETE_CONTACT,
  payload: {
    contactId,
  },
});
