export function throttle(func, ms) {
  let isPaused = false;
  let savedArgs;
  let savedThis;

  return function wrapper(...args) {
    if (isPaused) {
      savedArgs = args;
      savedThis = this;
      return;
    }

    func.apply(this, args);

    isPaused = true;
    setTimeout(() => {
      isPaused = false;
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedThis = null;
        savedArgs = null;
      }
    }, ms);
  };
}

export const saveToLocalStorage = contacts => {
  try {
    window.localStorage.setItem('contacts', JSON.stringify(contacts));
  } catch (error) {
    console.warn("Your browser doesn't support local storage");
  }
};
