import reducer, { initialState } from './address_reducer';
import {
  deleteContact,
  editContactField,
  selectContact,
  addContact,
  setFilterValue,
} from '../actions/address_actions';

describe('Address reducer', () => {
  it('can select the entry', () => {
    const currentState = {
      ...initialState,
    };
    const contactId = 'a0aef5c0-fba0-11e8-a9f7-df88c41052fb';
    const newState = reducer(currentState, selectContact(contactId));

    expect(newState.currentEntry.id).toEqual(contactId);
  });

  it('can edit the entry', () => {
    const currentState = {
      ...initialState,
    };
    const contactId = 'a0aef5c0-fba0-11e8-a9f7-df88c41052fb';

    let newState = reducer(currentState, selectContact(contactId));
    expect(newState.currentEntry.firstName).toBe('Adam');

    newState = reducer(newState, editContactField(contactId, 'firstName', 'Michael'));
    newState = reducer(newState, editContactField(contactId, 'lastName', 'Jackson'));
    newState = reducer(newState, editContactField(contactId, 'address', 'Some address'));
    expect(newState.contacts.find(item => item.id === contactId).firstName).toBe('Michael');
    expect(newState.contacts.find(item => item.id === contactId).lastName).toBe('Jackson');
    expect(newState.contacts.find(item => item.id === contactId).address).toBe('Some address');
    expect(newState.currentEntry.firstName).toBe('Michael');
    expect(newState.currentEntry.lastName).toBe('Jackson');
    expect(newState.currentEntry.address).toBe('Some address');
  });

  it('can remove the entry', () => {
    const currentState = {
      ...initialState,
    };
    const contactId = 'a0aef5c0-fba0-11e8-a9f7-df88c41052fb';
    expect(currentState.contacts.find(item => item.id === contactId)).not.toBeUndefined();

    const newState = reducer(currentState, deleteContact(contactId));
    expect(newState.contacts.find(item => item.id === contactId)).toBeUndefined();
    expect(newState.currentEntry).toBeNull();
  });

  it('can add the entry', () => {
    const currentState = {
      ...initialState,
    };

    let newState = {};

    const dispatch = argument => {
      newState = reducer(currentState, argument);
    };

    addContact()(dispatch);

    expect(newState.contacts.length).toBe(currentState.contacts.length + 1);
    expect(newState.currentEntry.firstName).toBe('New contact');
  });

  it('can set filter value', () => {
    const currentState = {
      ...initialState,
    };

    const newState = reducer(currentState, setFilterValue('Nicola'));
    expect(newState.filterValue).toBe('nicola');
  });
});
