import * as types from 'constants/action_types';
import uuidv1 from 'uuid/v1';
import { saveToLocalStorage } from 'helpers/helpers';

export const initialState = {
  contacts: [
    {
      id: '858c6570-fba0-11e8-96eb-9f66faa2d8b7',
      firstName: 'Nicola',
      lastName: 'Tesla',
      address: '1st Main Avenue, 142',
    },
    {
      id: 'a0aef5c0-fba0-11e8-a9f7-df88c41052fb',
      firstName: 'Adam',
      lastName: 'Miller',
      address: 'Corner of Schmidta and Shorsa',
    },
  ],
  currentEntry: null,
  filterValue: '',
  justCreated: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_CONTACTS:
      return {
        ...state,
        contacts: action.payload.contacts || initialState.contacts,
      };

    case types.EDIT_CONTACT_FIELD: {
      const contacts = state.contacts.map(item => {
        if (item.id === action.payload.contactId) {
          return {
            ...item,
            [action.payload.fieldName]: action.payload.fieldValue,
          };
        }
        return item;
      });

      saveToLocalStorage(contacts);

      return {
        ...state,
        contacts,
        currentEntry: {
          ...state.currentEntry,
          [action.payload.fieldName]: action.payload.fieldValue,
        },
      };
    }

    case types.SELECT_CONTACT:
      return {
        ...state,
        currentEntry:
          state.currentEntry && state.currentEntry.id === action.payload.contactId
            ? null
            : state.contacts.find(item => item.id === action.payload.contactId),
      };

    case types.ADD_CONTACT: {
      const newContact = {
        id: uuidv1(),
        firstName: 'New contact',
        lastName: '',
        address: '',
      };

      const contacts = [
        ...state.contacts,
        {
          ...newContact,
        },
      ];

      saveToLocalStorage(contacts);

      return {
        ...state,
        currentEntry: {
          ...newContact,
        },
        contacts,
        justCreated: true,
      };
    }

    case types.RESET_JUST_CREATED:
      return {
        ...state,
        justCreated: false,
      };

    case types.SET_FILTER_VALUE:
      return {
        ...state,
        filterValue: action.payload.filterValue.toLowerCase(),
      };

    case types.DELETE_CONTACT: {
      const contacts = state.contacts.filter(item => item.id !== action.payload.contactId);

      saveToLocalStorage(contacts);

      return {
        ...state,
        currentEntry: null,
        contacts,
      };
    }

    default:
      return state;
  }
};
