import { combineReducers } from 'redux';
import address from './address_reducer';

export default combineReducers({
  address,
});
