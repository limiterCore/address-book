import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import reducer from 'reducers/root_reducer';
import thunk from 'redux-thunk';

/** Make middlwares list */
const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
  const loggerMiddleware = createLogger({
    duration: true,
    diff: true,
    collapsed: true,
  });

  middlewares.push(loggerMiddleware);
}

export default createStore(reducer, compose(applyMiddleware(...middlewares)));
