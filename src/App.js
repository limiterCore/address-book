import React from 'react';
import { Provider } from 'react-redux';

// Store
import store from 'store';

// Components
import AddressBook from 'components/AddressBook';

const App = () => (
  <Provider store={store}>
    <AddressBook />
  </Provider>
);

export default App;
